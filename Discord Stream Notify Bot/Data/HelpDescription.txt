﻿由 <@284989733229297664>(孤之界#1121) 製作

可以通知直播待機所建立/開台/關台(部分頻道)
以及影片上傳通知(非主要功能)
邀請Bot後請開一個新的文字頻道做為直播通知用，並且開放Bot的讀取、發送訊息權限(最好的做法是不要變更預設權限或直接給全權)
以下提到的所有指令都可以用 `/help get-command-help 指令` 查看詳細解說

__**1.YouTube直播設定**__
首先，找到你的推的頻道網址，並在**__你準備當作直播通知的頻道__**執行 `/youtube add-youtube-notice 頻道網址 發送通知的頻道`
(如果要加入複數頻道可執行多次 `/youtube add-youtube-notice 頻道網址 發送通知的頻道` )
如果顯示 `該頻道未加入爬蟲清單`，就執行 `/youtube add-youtube-spider 頻道網址` 後再執行 `/youtube add-youtube-notice 頻道網址 發送通知的頻道`
(注意: 請勿加入非V頻道，詳細規則請執行 `/help get-command-help add-youtube-spider` )
如果你比較博愛，也可以用 `all` `holo` `2434` 取代頻道網址

接著，可以使用 `/youtube set-youtube-notice-message` 設定通知訊息，例如tag特定身分組等等
設定舉例: `/youtube set-youtube-notice-message 頻道網址 start @直播通知 開台了!!`

最後，可以用 `/youtube list-youtube-notice` 與 `/youtube list-youtube-notice-message` 確認設定狀況

__**2.YouTube直播移除**__
如果你設定錯誤或想改變設定，可以依照以下方法執行
1.取消通知: `/youtube remove-youtube-notice 頻道網址`
2.通知訊息設定: `/youtube set-youtube-notice-message` 系列指令執行時會直接覆蓋前次設定，相關用法請看 `/help get-command-help set-youtube-notice-message`

__**3.TwitterSpace設定**__
基本上跟YouTube的設定大同小異
詳情請輸入 `/help get-all-commands twitter-space` 和 `/help get-command-help add-space-notice`

__**4.其他特殊功能**__
部分頻道會有錄影，可以使用 `/youtube now-record-channel` 跟 `/youtube list-record-channel` 查詢
如果伺服器Boost達Lv2的話可以使用 `/youtube set-banner-change 頻道網址`，設定伺服器橫幅使用指定頻道的最新影片(直播)縮圖
(可用 `/help get-command-help set-banner-change` 查看詳情)

[點此邀請BOT到新伺服器](https://discordapp.com/api/oauth2/authorize?client_id=758222559392432160&permissions=2416143425&scope=bot%20applications.commands)

有新功能想加入或任何建議可以到推特私訊 [孤之界(@Jun112561)](https://twitter.com/Jun112561) 討論